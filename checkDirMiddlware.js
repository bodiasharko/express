const fs = require('fs');

module.exports = (req, res, next) => {
  if (!fs.existsSync(`${__dirname}/files`)) {
    fs.mkdirSync(`${__dirname}/files`);
  }
  next();
};
