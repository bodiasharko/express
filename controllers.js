const fs = require('fs');
const path = require('path');

class Controllers {
  createFile(req, res) {
    const { filename, content } = req.body;
    try {
      fs.readFile(`${__dirname}/files/${filename}`, (err) => {
        if (!err) {
          res.status(500).json({ message: 'Server error' });
        } else {
          fs.writeFile(`${__dirname}/files/${filename}`, content, (e) => {
            if (!e) {
              res.status(200).json({ message: 'File created successfully' });
            } else {
              res
                .status(400)
                .json({ message: "Please specify 'content' parameter" });
            }
          });
        }
      });
    } catch (e) {
      res.status(500).json({ message: 'Server error' });
    }
  }

  getAllFiles(req, res) {
    try {
      fs.readdir(`${__dirname}/files`, (err, files) => {
        if (!err) {
          res.status(200).json({
            message: 'Success',
            files,
          });
        } else {
          res.status(400).json({ message: 'Client error' });
        }
      });
    } catch (e) {
      res.status(500).json({ message: 'Server error' });
    }
  }

  getFile(req, res) {
    try {
      const { filename } = req.params;
      fs.readFile(`${__dirname}/files/${filename}`, 'utf-8', (err, data) => {
        if (err) {
          res
            .status(400)
            .json({ message: `No file with '${filename}' filename found` });
        } else {
          res.status(200).json({
            message: 'Success',
            filename,
            content: data,
            extension: path.extname(filename),
            uploadedDate: fs.statSync(`${__dirname}/files/${filename}`)
              .birthtime,
          });
        }
      });
    } catch (e) {
      res.status(500).json({ message: 'Server error' });
    }
  }

  updateFile(req, res) {
    try {
      const { filename, content } = req.body;
      fs.readFile(`${__dirname}/files/${filename}`, (err) => {
        if (err) {
          res
            .status(400)
            .json({ message: `No file with ${filename} filename found` });
        } else {
          fs.writeFile(`${__dirname}/files/${filename}`, content, (e) => {
            if (!e) {
              res.status(200).json({ message: 'Success' });
            } else {
              res.status(500).json({ message: `Server error` });
            }
          });
        }
      });
    } catch (e) {
      res.status(500).json({ message: 'Server error' });
    }
  }

  deleteFile(req, res) {
    try {
      const { filename } = req.params;
      fs.unlink(`${__dirname}/files/${filename}`, (err) => {
        if (err) {
          res.status(400).json({ message: 'File not found' });
        } else {
          res.status(200).json({ message: 'Success' });
        }
      });
    } catch (e) {
      res.status(500).json({ message: 'Server error' });
    }
  }
}

module.exports = new Controllers();
