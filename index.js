require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const controllers = require('./controllers');
const checkDirMiddleware = require('./checkDirMiddlware');

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('combined'));

app.post('/api/files', checkDirMiddleware, controllers.createFile);
app.get('/api/files', controllers.getAllFiles);
app.put('/api/files', controllers.updateFile);
app.get('/api/files/:filename', controllers.getFile);
app.delete('/api/files/:filename', controllers.deleteFile);

app.listen(process.env.PORT, () => {
  console.log(`Server started on port: ${process.env.PORT}`);
});
